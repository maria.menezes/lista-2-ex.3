
/**
 * Escreva a descrição da classe Gabarito aqui.
 * 
 * @author (seu nome) 
 * @version (número de versão ou data)
 */
public class Gabarito
{
    private int coluna1, coluna0, coluna2;
    private String resultado;
    
    public Gabarito()
    {
        coluna1=0;
        coluna0=0;
        coluna2=0;
        resultado="";
    }
    public int Colunas()
    {
        if (resultado=="vencedor")
        {
            return coluna1;
        }
        else if (resultado=="empate")
        {
            return coluna0;
        }
        else if (resultado=="segundo vencedor")
        {
            return coluna2;
        }
        else
        {
            return 0;
        }
    }
}
